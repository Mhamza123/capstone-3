import React from 'react';
import {Container, Columns, Card, Heading, Section, Hero, Button, Image} from "react-bulma-components"
import { Link } from "react-router-dom";
import Reservation from "./Reservation.js";


const WorkSpace = (props) => {
  return (
    <div>
    	<Hero color="danger" size="medium" gradient  >
	          <Hero.Body>
	            <Container>
	              <Heading>
	                WorkSpace
	              </Heading>
	              <Heading subtitle size={3}>
	              	Choose a workspace that suit your preference
	              </Heading>
	            </Container>
	          </Hero.Body>
	        </Hero>
			{/*Jumbotron Image*/}

			<Container fluid className="mt-5">
				<Heading className="text-center">Choose your type of WorkSpace</Heading>
			</Container>
		{/*Heading Text*/}

			{/*Image Slider for Types of Room*/}
			<Container fluid className="mt-5">
				<Columns>
			        <Columns.Column size={12}>
			         	<Columns>
			         	<Columns.Column size={6}>
				         	<Card>
					            <Card.Content className="p-0">
					               <Image size={256} alt="64x64" src="/images/homepage.jpg" />
					            </Card.Content>
					            <Card.Header>
					              <Card.Header.Title>Type of User</Card.Header.Title>
					            </Card.Header>
					            <Card.Content>
					                <Link to="/workspace/reservation" className="navbar-item">
           								<Button color="primary" fullwidth>View</Button>
          							</Link>
					            </Card.Content>
					        </Card>
			         	</Columns.Column>
			         	<Columns.Column size={6}>
				         	<Card>
					            <Card.Content className="p-0">
					               <Image size={256} alt="64x64" src="/images/homepage.jpg" />
					            </Card.Content>
					            <Card.Header>
					              <Card.Header.Title>Type of User</Card.Header.Title>
					            </Card.Header>
					            <Card.Content>
					               <Button color="primary" fullwidth> 
					               View
					               </Button>
					            </Card.Content>
					        </Card>
			         	</Columns.Column>
			         </Columns>

			        </Columns.Column>
			      </Columns>
		    </Container>

		    
			    
		    
    </div>
  )
}

export default WorkSpace;