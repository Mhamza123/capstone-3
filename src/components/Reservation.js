import React from "react";
import {
	Container,
	Columns,
	Card,
	Button,
	Loader,
	Heading,
	Modal,
	Section,
	Image
} from "react-bulma-components";
import PropTypes from 'prop-types';

class OpenModal extends React.Component {
  static propTypes = {
    modal: PropTypes.object,
    children: PropTypes.node.isRequired,
  }

  static defaultProps = {
    modal: {},
  }

  state = {
    show: false,
  }

  open = () => this.setState({ show: true });
  close = () => this.setState({ show: false });

  render() {
    return (
      <div>
        <Button onClick={this.open}>Reserve A WorkSpace</Button>
        <Modal show={this.state.show} onClose={this.close} {...this.props.modal}>
          {this.props.children}
        </Modal>
      </div>
    );
  }
}

const Reservation = () => {
	return (
		<Container>
			<Columns className="mt-2">
				<Columns.Column size={8}>
			          <Card>
			            <Card.Header>
			              <Card.Header.Title>
			              <Heading size={4}>Shared Work	Space</Heading>
			              </Card.Header.Title>
			            </Card.Header>
			            <Card.Content className="p-0">
					               <Image size={256} alt="64x64" src="/images/homepage.jpg" />
					    </Card.Content>
			          </Card>
			        </Columns.Column>
				<Columns.Column size={4}>
					<Card>
						<Card.Header>
							<Card.Header.Title>
								<Heading size={4}>Details</Heading>
							</Card.Header.Title>
						</Card.Header>
						<Card.Content>
								<Heading subtitle size={6}>
			   						 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
			   					</Heading>
			   					<Heading subtitle size={6}>
			   						 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
			   					</Heading>
			   					<Heading subtitle size={6}>
			   						 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
			   					</Heading>
						</Card.Content>
						 <Card.Footer>
					     
					        <OpenModal modal={{ closeOnEsc: false }}>
						      <Modal.Content>
						        <Section style={{ backgroundColor: 'white' }}>
						          	<form>
										<div className="field">
											<label htmlFor="dateFrom">Date From</label>
											<input
												id="dateFrom"
												className="input"
												type="date"
											/>
										</div>
										{/* From */}

										<div className="field">
											<label htmlFor="dateTo">Date To</label>
											<input
												id="dateTo"
												className="input"
												type="date"
											/>
										</div>
										{/* To */}


										<Button type="button" color="success" fullwidth>
											Submit
										</Button>
									</form>
						        </Section>
						      </Modal.Content>
						    </OpenModal>
					     </Card.Footer>
					</Card>
				</Columns.Column>
			</Columns>

		</Container>
	);
};

export default Reservation;
