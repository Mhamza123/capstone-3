import React from "react";
import {
	Container,
	Columns,
	Card,
	Button,
	Loader,
	Heading
} from "react-bulma-components";

const Login = () => {
	return (
		<Container>
			<Columns>
				<Columns.Column
					className="mt-5"
					size="half"
					offset="one-quarter"
				>
					<Card>
						<Card.Header>
							<Card.Header.Title>
								<Heading>Login</Heading>
							</Card.Header.Title>
						</Card.Header>
						<Card.Content>
							<form>
								<div className="field">
									<label htmlFor="username"> Username </label>
									<input
										id="username"
										className="input"
										type="text"
									/>
								</div>
								{/* Username */}

								<div className="field">
									<label htmlFor="password">Password</label>
									<input
										id="password"
										className="input"
										type="password"
									/>
								</div>
								{/* Password */}
								<Button type="button" color="success" fullwidth>
									Submit
								</Button>
							</form>
						</Card.Content>
					</Card>
				</Columns.Column>
			</Columns>
		</Container>
	);
};

export default Login;
