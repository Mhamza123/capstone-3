import React, { useState } from "react";
import { Container, Columns, Card, Button } from "react-bulma-components";
import { graphql } from "react-apollo";

import Swal from "sweetalert2";
import { flowRight as compose } from "lodash";
import { Link } from "react-router-dom";

import { getRoomsQuery } from "../queries/queries.js";
import { createRoomMutation } from "../queries/mutations.js";
import { deleteRoom } from "../queries/mutations.js";

const Room = props => {
	console.log(props);
	const deleteClickHandler = e => {
		e.preventDefault();
		let deleteRoom = {
			id: e.target.value
		};
		console.log(deleteRoom);

		Swal.fire({
			title: "Are you sure?",
			icon: "warning",
			showCancelButton: true,
			confirmButtonText: "Delete"
		}).then(result => {
			if (result.value) {
				props.deleteRoomCategory({
					variables: deleteRoom,
					refetchQueries: [{ query: getRoomsQuery }]
				});
				console.log(deleteRoom);
				Swal.fire("Deleted!", "success");
			}
		});
	};

	console.log(props.data);
	const [roomNum, setRoomNum] = useState("");
	const [roomType, setRoomType] = useState("");
	const [numOfpeople, setNumOfPeople] = useState("");
	let data = props.data;

	const roomNumChangeHandler = e => {
		setRoomNum(e.target.value);
		console.log(roomNum);
	};

	const roomTypeChangeHandler = e => {
		setRoomType(e.target.value);
		console.log(roomType);
	};

	const numOfPeopleChangeHandler = e => {
		setNumOfPeople(e.target.value);
		console.log(numOfpeople);
	};

	let roomList;
	let roomTypeList;

	if (data.getRooms === undefined) {
		roomList = (
			<tr>
				<td>Fetching Room....</td>
			</tr>
		);
	} else {
		roomList = data.getRooms.map(room => {
			return (
				<tr key={room.id}>
					<td>{room.roomNumber}</td>
					<td>{room.roomType.roomType}</td>
					<td>{room.numOfPeople}</td>
					<td>
						<Link to={"/admin/updateroom/" + room.id}>
							<Button color="link" fullwidth>
								Update
							</Button>
						</Link>
						<Button
							value={roomType.id}
							onClick={deleteClickHandler}
							color="danger"
							fullwidth
						>
							Delete
						</Button>
					</td>
				</tr>
			);
		});
	}

	if (data.getRoomCategories === undefined) {
		roomTypeList = <option selected>fetching teams...</option>;
	} else {
		roomTypeList = data.getRoomCategories.map(type => {
			// console.log(team);
			return (
				<option key={type.id} value={type.id}>
					{type.roomType}
				</option>
			);
		});
	}

	return (
		<Container>
			<Columns>
				<Columns.Column size={3}>
					<Card>
						<Card.Header>
							<Card.Header.Title>Add Task</Card.Header.Title>
						</Card.Header>

						<Card.Content>
							<form>
								<div className="field">
									<label
										className="label"
										htmlFor="description"
									>
										roomNumber
									</label>
									<input
										id="description"
										className="input"
										type="text"
									/>
								</div>
								{/* end of field*/}

								<div className="field">
									<label
										className="label"
										htmlFor="numOfPeople"
									>
										roomNumber
									</label>
									<input
										id="description"
										className="input"
										type="text"
									/>
								</div>

								<div className="field">
									<label>Room Type</label>
									<div className="control">
										<div className="select is-fullwidth">
											<select
												onClick={roomTypeChangeHandler}
											>
												<option
													selected
													value="default"
												>
													Select Room Type
												</option>
											</select>
										</div>
									</div>
								</div>

								<Button type="submit" color="success" fullwidth>
									Add New Team
								</Button>
							</form>
						</Card.Content>
					</Card>
				</Columns.Column>

				<Columns.Column size={9}>
					<Card>
						<Card.Header>
							<Card.Header.Title>Task List</Card.Header.Title>
						</Card.Header>

						<Card.Content>
							<div className="table-container">
								<table className="table is-fullwidth is-bordered">
									<thead>
										<th>Room Number</th>
										<th># of People</th>
										<th>Room Type</th>
										<th>Actions</th>
									</thead>
									<tbody>{roomList}</tbody>
								</table>
							</div>
						</Card.Content>
					</Card>
				</Columns.Column>
			</Columns>
		</Container>
	);
};

export default compose(
	graphql(getRoomsQuery),
	graphql(deleteRoom, { name: "deleteRoom" }),
	graphql(createRoomMutation, { name: "createRoom" })
)(Room);
