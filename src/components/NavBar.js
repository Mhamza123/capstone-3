import React, { useState } from "react";
import { Navbar } from "react-bulma-components";
import { Link } from "react-router-dom";

const NavBar = props => {
  const [open, setOpen] = useState(false);
  const Burger = () => {
    setOpen(!open);
    console.log("Galaw");
  };
  return (
    <Navbar color="black" active={open}>
      <Navbar.Brand>
        <Navbar.Item>WorkHUB</Navbar.Item>
        <Navbar.Burger onClick={Burger} />
      </Navbar.Brand>

      <Navbar.Menu>
        <Navbar.Container position="end">
          <Link to="/" className="navbar-item">
            Home
          </Link>

          <Link to="/workspace" className="navbar-item">
            WorkSpace
          </Link>

        
         

          <Link to="/login" className="navbar-item">
            Log-in
          </Link>

          <Link to="/registration" className="navbar-item">
            Sign-Up
          </Link>
        </Navbar.Container>
      </Navbar.Menu>
    </Navbar>
  );
};

export default NavBar;
