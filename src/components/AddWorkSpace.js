import React from 'react';
import {
	Container,
	Columns,
	Card,
	Button,
	Loader,
	Heading,
	Modal,
	Section,
	Image
} from "react-bulma-components";import PropTypes from 'prop-types';

class OpenModal extends React.Component {
  static propTypes = {
    modal: PropTypes.object,
    children: PropTypes.node.isRequired,
  }

  static defaultProps = {
    modal: {},
  }

  state = {
    show: false,
  }

  open = () => this.setState({ show: true });
  close = () => this.setState({ show: false });

  render() {
    return (
      <div>
        <Button onClick={this.open}>Reserve A WorkSpace</Button>
        <Modal show={this.state.show} onClose={this.close} {...this.props.modal}>
          {this.props.children}
        </Modal>
      </div>
    );
  }
}

const AddWorkSpace = (props) => {
  return (
		<Container>
			<Columns>
				<Columns.Column size={3}>
					{/*form left side*/}
					<Card>
						<Card.Header>
							<Card.Header.Title>Add WorkSpace</Card.Header.Title>
						</Card.Header>
						<Card.Content>
							<form>
								<div className="field">
									<label htmlFor="workSpacename">WorkSpaceName</label>
									<input
										type="text"
										id="workSpacename"
										className="input"
										
									/>
								</div>

								<div className="field">
									<label htmlFor="description">Description</label>
									<input
										type="text"
										id="description"
										className="input"
										
									/>
								</div>

								<div className="field">
									<label htmlFor="description">Room Number</label>
									<input
										type="text"
										id="description"
										className="input"
										
									/>
								</div>

								<div className="field">
									<label htmlFor="description">Room Type</label>
									<input
										type="text"
										id="description"
										className="input"
										
									/>
								</div>

								<div className="field">
									<label htmlFor="description">Price</label>
									<input
										type="text"
										id="description"
										className="input"
										
									/>
								</div>
								<Button type="submit" color="success" fullwidth>
									Create Team
								</Button>
							</form>
						</Card.Content>
					</Card>
				</Columns.Column>
				<Columns.Column size={9}>
					{/* list right side */}
					<Card>
						<Card.Header>
							<Card.Header.Title>WorkSpace</Card.Header.Title>
						</Card.Header>
						<Card.Content>
							<div className="table-container">
								<table className="table is-fullwidth">
									<thead>
										<tr>
											<th>WorkSpaceName</th>
											<th>Description</th>
											<th>Room Number</th>
											<th>Room Type</th>
											<th>Price </th>
											<th>Actions</th>
										</tr>
									</thead>
									<tbody></tbody>
								</table>
							</div>
						</Card.Content>
					</Card>
				</Columns.Column>
			</Columns>
		</Container>
  );
}

export default AddWorkSpace;