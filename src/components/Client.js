import React from "react";
import {
  Container,
  Columns,
  Card,
  Button,
  Loader
} from "react-bulma-components";
import { graphql } from "react-apollo";
import { flowRight as compose } from "lodash";
import { Link } from "react-router-dom";
import Swal from "sweetalert2";

import { getRegistrationsQuery } from "../queries/queries";

import { deleteRegistration } from "../queries/mutations.js";
const Client = props => {
  console.log(props);
  let data = props.data;

  const deleteRegistrationClickHandler = e => {
    e.preventDefault();
    let deleteRegistration = {
      id: e.target.value
    };
    // console.log(deletedVenue)
    Swal.fire({
      title: "Are you sure?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonText: "Delete"
    }).then(result => {
      if (result.value) {
        props.deleteRegistration({
          variables: deleteRegistration,
          refetchQueries: [{ query: getRegistrationsQuery }]
        });
        Swal.fire("Deleted!", "success");
      }
    });
  };

  let clientList;
  // we are still fetching from the backend
  if (data.getRegistrations === undefined) {
    console.log("fetching users...");
    clientList = (
      <tr>
        <Loader style={{ display: "inline-block" }} /> Fetching users....
      </tr>
    );
  } else {
    console.log("registration are loaded");

    clientList = data.getRegistrations.map(registration => {
      // console.log(registration);

      return (
        <tr key={registration.id}>
          <td>{registration.firstname}</td>
          <td>{registration.lastname}</td>
          <td>{registration.email}</td>
          <td>{registration.address}</td>
          <td>{registration.phone}</td>
          <td>
            <Link to={"/admin/updateregistration/" + registration.id}>
              <Button color="link" fullwidth>
                Update
              </Button>
            </Link>

            <Button
              value={registration.id}
              onClick={deleteRegistrationClickHandler}
              color="danger"
              fullwidth
            >
              Delete
            </Button>
          </td>
        </tr>
      );
    });
  }

  return (
    <Container>
      <Columns>
        <Columns.Column size={12}>
          <Card>
            <Card.Header>
              <Card.Header.Title>Client List</Card.Header.Title>
            </Card.Header>
            <Card.Content>
              <div className="table-container">
                <table className="table is-fullwidth">
                  <thead>
                    <tr>
                      <th>Firsname</th>
                      <th>Lastname</th>
                      <th>email</th>
                      <th>address</th>
                      <th>mobile number</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>{clientList}</tbody>
                </table>
              </div>
            </Card.Content>
          </Card>
        </Columns.Column>
      </Columns>
    </Container>
  );
};

export default compose(
  graphql(getRegistrationsQuery),
  graphql(deleteRegistration, { name: "deleteRegistration" })
)(Client);
