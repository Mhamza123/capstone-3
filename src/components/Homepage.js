import React from "react";
import {Container, Columns, Card, Heading, Section, Hero, Button, Image} from "react-bulma-components"

const Homepage = () => {

	return(
		<div>
			<Hero color="primary" size="medium" gradient  >
	          <Hero.Body>
	            <Container>
	              <Heading>
	                WorkHub
	              </Heading>
	              <Heading subtitle size={3}>
	              	Choose a workspace that suit your preference
	              </Heading>
	            </Container>
	          </Hero.Body>
	        </Hero>
			{/*Jumbotron Image*/}

			{/*Image Slider for Types of Room*/}
			<Container fluid className="mt-5">
				<Columns>
			        <Columns.Column size={3}>
			          <Card>
			            <Card.Header>
			              <Card.Header.Title>
			              <Heading size={4}>Room</Heading>
			              </Card.Header.Title>
			            </Card.Header>
			            <Card.Content>
			   
			            </Card.Content>
			          </Card>
			        </Columns.Column>
			        <Columns.Column size={9}>
			         	<Columns>
			         	<Columns.Column size={6}>
				         	<Card>
					            <Card.Header>
					              <Card.Header.Title>Type of User</Card.Header.Title>
					            </Card.Header>
					            <Card.Content>
					               <Image size={128} alt="64x64" src="http://bulma.io/images/placeholders/128x128.png" />
					       
					            </Card.Content>
					        </Card>
			         	</Columns.Column>
			         	<Columns.Column size={6}>
				         	<Card>
					            <Card.Header>
					              <Card.Header.Title>Type of User</Card.Header.Title>
					            </Card.Header>
					            <Card.Content>
					              <Image size={128} alt="64x64" src="http://bulma.io/images/placeholders/128x128.png" />
					       
					            </Card.Content>
					        </Card>
			         	</Columns.Column>
			         </Columns>

			        </Columns.Column>
			      </Columns>
		    </Container>

		    {/*Amenties for choosing our Workspace*/}
		    <Container fluid className="mt-5">
				<Columns>
			        <Columns.Column size={3}>
			          <Card>
			            <Card.Header>
			              <Card.Header.Title>
			              <Heading size={4}>Amenities</Heading>
			              </Card.Header.Title>
			            </Card.Header>
			            <Card.Content>
			   
			            </Card.Content>
			          </Card>
			        </Columns.Column>
			        <Columns.Column size={9}>
			         	<Columns>
			         	<Columns.Column size={6}>
				         	<Card>
					            <Card.Header>
					              <Card.Header.Title>Type of User</Card.Header.Title>
					            </Card.Header>
					            <Card.Content>
					               <Image size={128} alt="64x64" src="http://bulma.io/images/placeholders/128x128.png" />
					       
					            </Card.Content>
					        </Card>
			         	</Columns.Column>
			         	<Columns.Column size={6}>
				         	<Card>
					            <Card.Header>
					              <Card.Header.Title>Type of User</Card.Header.Title>
					            </Card.Header>
					            <Card.Content>
					              <Image size={128} alt="64x64" src="http://bulma.io/images/placeholders/128x128.png" />
					       
					            </Card.Content>
					        </Card>
			         	</Columns.Column>
			         </Columns>

			        </Columns.Column>
			      </Columns>
		    </Container>


		</div>
	
		)
}

export default Homepage