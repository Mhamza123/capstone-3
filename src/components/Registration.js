import React, { useState } from "react";
import {
	Container,
	Columns,
	Card,
	Button,
	Heading
} from "react-bulma-components";
import { graphql } from "react-apollo";
import Swal from "sweetalert2";
import { flowRight as compose } from "lodash";

import { getRegistrationsQuery } from "../queries/queries.js";
import { createRegistrationMutation } from "../queries/mutations.js";

const Registration = props => {
	const [firstname, setFirstName] = useState("Input first name here");
	const [lastname, setLastName] = useState("Input last name here");
	const [email, setEmail] = useState("Input Email here");
	const [address, setAddress] = useState("Input Address here");
	const [phone, setPhone] = useState("Input Phone here");
	const [password, setPassword] = useState("input password here");
	const [username, setUsername] = useState("input username here");

	const firstNameChangeHandler = e => {
		setFirstName(e.target.value);
		console.log(firstname);
	};

	const lastNameChangeHandler = e => {
		setLastName(e.target.value);
		console.log(lastname);
	};

	const emailChangeHandler = e => {
		setEmail(e.target.value);
		console.log(email);
	};

	const addressChangeHandler = e => {
		setAddress(e.target.value);
		console.log(address);
	};

	const phoneChangeHandler = e => {
		setPhone(e.target.value);
		console.log(phone);
	};

	const usernameChangeHandler = e => {
		setUsername(e.target.value);
		console.log(username);
	};

	const passwordChangeHandler = e => {
		setPassword(e.target.value);
		console.log(password);
	};

	const addUserClickHandler = e => {
		let newRegistration = {
			firstname: firstname,
			lastname: lastname,
			email: email,
			address: address,
			phone: phone,
			username: username,
			password: password
		};

		console.log(newRegistration);

		props.createRegistration({
			variables: newRegistration,
			refetchQueries: [{ query: getRegistrationsQuery }]
		});

		// insert SWAL here
		Swal.fire({
			icon: "success",
			title: "Successfully added a new user"
		});
	};

	return (
		<Container>
			<Columns>
				<Columns.Column
					className="mt-5"
					size="half"
					offset="one-quarter"
				>
					<Card>
						<Card.Header>
							<Card.Header.Title>
								<Heading>Registration</Heading>
							</Card.Header.Title>
						</Card.Header>
						<Card.Content>
							<form>
								<div className="field">
									<label htmlFor="fName">First name</label>
									<input
										id="fName"
										className="input"
										type="text"
										onChange={firstNameChangeHandler}
										required
									/>
								</div>
								{/* Firstname */}

								<div className="field">
									<label htmlFor="lName">Last name</label>
									<input
										id="lName"
										className="input"
										type="text"
										onChange={lastNameChangeHandler}
										required
									/>
								</div>
								{/* Lastname */}

								<div className="field">
									<label htmlFor="email">Email Address</label>
									<input
										id="email"
										className="input"
										type="email"
										onChange={emailChangeHandler}
										required
									/>
								</div>
								{/* Email */}

								<div className="field">
									<label htmlFor="address">Address</label>
									<input
										id="address"
										className="input"
										type="text"
										onChange={addressChangeHandler}
										required
									/>
								</div>
								{/* Address */}

								<div className="field">
									<label htmlFor="address">
										Mobile Number
									</label>
									<input
										id="phone"
										className="input"
										type="tel"
										onChange={phoneChangeHandler}
										required
									/>
								</div>
								{/* Phone */}

								<div className="field">
									<label htmlFor="username"> Username </label>
									<input
										id="username"
										className="input"
										type="text"
										onChange={usernameChangeHandler}
										required
									/>
								</div>
								{/* Username */}

								<div className="field">
									<label htmlFor="password">Password</label>
									<input
										id="password"
										className="input"
										type="password"
										onChange={passwordChangeHandler}
										required
									/>
								</div>
								{/* Password */}

								<Button
									type="button"
									color="success"
									onClick={addUserClickHandler}
									fullwidth
								>
									Submit
								</Button>
							</form>
						</Card.Content>
					</Card>
				</Columns.Column>
			</Columns>
		</Container>
	);
};

export default compose(
	graphql(getRegistrationsQuery),
	graphql(createRegistrationMutation, { name: "createRegistration" })
)(Registration);
