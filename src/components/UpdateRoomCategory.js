import React, { useState } from "react";
import {
	Container,
	Columns,
	Card,
	Button,
	Heading
} from "react-bulma-components";
import { graphql } from "react-apollo";
import Swal from "sweetalert2";
import { flowRight as compose } from "lodash";

import { getRoomCategoryQuery } from "../queries/queries.js";
import { getRoomCategoriesQuery } from "../queries/queries.js";
import { updateRoomCategory } from "../queries/mutations.js";

const UpdateRoomCategory = props => {
	console.log(props);
	const [roomType, setRoomType] = useState("");

	let data = props.data;

	const roomTypeChangeHandler = e => {
		setRoomType(e.target.value);
		console.log(roomType);
	};

	if (data.getRoomCategory === undefined) {
		console.log("fetching the users");
	} else {
		const setDefaultValues = () => {
			setRoomType(data.getRoomCategory.roomType);
		};

		if (roomType === "") {
			setDefaultValues();
		}
	}

	const FormSubmitHandler = e => {
		let updateRoomCategory = {
			id: props.match.params.id,
			roomType: roomType
		};

		console.log(updateRoomCategory);

		props.updateRoomCategory({
			variables: updateRoomCategory,
			refetchQueries: [{ query: getRoomCategoriesQuery }]
		});
	};

	return (
		<Container>
			<Columns>
				<Columns.Column
					className="mt-5"
					size="half"
					offset="one-quarter"
				>
					<Card>
						<Card.Header>
							<Card.Header.Title>
								<Heading>Update</Heading>
							</Card.Header.Title>
						</Card.Header>
						<Card.Content>
							<form onSubmit={FormSubmitHandler}>
								<div className="field">
									<label htmlFor="fName">Room Type</label>
									<input
										id="fName"
										className="input"
										type="text"
										value={roomType}
										onChange={roomTypeChangeHandler}
									/>
								</div>
								{/* Firstname */}

								<Button type="submit" color="success" fullwidth>
									Submit
								</Button>
							</form>
						</Card.Content>
					</Card>
				</Columns.Column>
			</Columns>
		</Container>
	);
};

export default compose(
	graphql(updateRoomCategory, { name: "updateRoomCategory" }),
	graphql(getRoomCategoriesQuery, { name: "getRoomCategories" }),
	graphql(getRoomCategoryQuery, {
		options: props => {
			console.log(props.match.params.id);
			return {
				variables: { id: props.match.params.id }
			};
		}
	})
)(UpdateRoomCategory);
