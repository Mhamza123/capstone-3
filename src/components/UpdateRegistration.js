import React, { useState } from "react";
import {
	Container,
	Columns,
	Card,
	Button,
	Heading
} from "react-bulma-components";
import { graphql } from "react-apollo";
import Swal from "sweetalert2";
import { flowRight as compose } from "lodash";

import { getRegistrationQuery } from "../queries/queries.js";
import { getRegistrationsQuery } from "../queries/queries.js";
import { updateRegistration } from "../queries/mutations.js";

const UpdateRegistration = props => {
	console.log(props.data);
	const [firstname, setFirstName] = useState("");
	const [lastname, setLastName] = useState("");
	const [email, setEmail] = useState("");
	const [address, setAddress] = useState("");
	const [phone, setPhone] = useState("");

	let data = props.data;

	const firstNameChangeHandler = e => {
		setFirstName(e.target.value);
		console.log(firstname);
	};

	const lastNameChangeHandler = e => {
		setLastName(e.target.value);
		console.log(lastname);
	};

	const emailChangeHandler = e => {
		setEmail(e.target.value);
		console.log(email);
	};

	const addressChangeHandler = e => {
		setAddress(e.target.value);
		console.log(address);
	};

	const phoneChangeHandler = e => {
		setPhone(e.target.value);
		console.log(phone);
	};

	if (data.getRegistration === undefined) {
		console.log("fetching the users");
	} else {
		const setDefaultValues = () => {
			setFirstName(data.getRegistration.firstname);
			setLastName(data.getRegistration.lastname);
			setEmail(data.getRegistration.email);
			setAddress(data.getRegistration.address);
			setPhone(data.getRegistration.phone);
		};

		if (firstname === "") {
			setDefaultValues();
		}
	}

	const FormSubmitHandler = e => {
		let updateRegistration = {
			id: props.match.params.id,
			firstname: firstname,
			lastname: lastname,
			email: email,
			address: address,
			phone: phone
		};

		console.log(updateRegistration);

		props.updateRegistration({
			variables: updateRegistration,
			refetchQueries: [{ query: getRegistrationsQuery }]
		});

		// we are accessing the history prop of react-router-dom
		// the goBack method goes back to the previous component
		props.history.goBack();
	};

	return (
		<Container>
			<Columns>
				<Columns.Column
					className="mt-5"
					size="half"
					offset="one-quarter"
				>
					<Card>
						<Card.Header>
							<Card.Header.Title>
								<Heading>Registration</Heading>
							</Card.Header.Title>
						</Card.Header>
						<Card.Content>
							<form onSubmit={FormSubmitHandler}>
								<div className="field">
									<label htmlFor="fName">First name</label>
									<input
										id="fName"
										className="input"
										type="text"
										value={firstname}
										onChange={firstNameChangeHandler}
										required
									/>
								</div>
								{/* Firstname */}

								<div className="field">
									<label htmlFor="lName">Last name</label>
									<input
										id="lName"
										className="input"
										type="text"
										value={lastname}
										onChange={lastNameChangeHandler}
										required
									/>
								</div>
								{/* Lastname */}

								<div className="field">
									<label htmlFor="email">Email Address</label>
									<input
										id="email"
										className="input"
										type="email"
										value={email}
										onChange={emailChangeHandler}
										required
									/>
								</div>
								{/* Email */}

								<div className="field">
									<label htmlFor="address">Address</label>
									<input
										id="address"
										className="input"
										type="text"
										value={address}
										onChange={addressChangeHandler}
										required
									/>
								</div>
								{/* Address */}

								<div className="field">
									<label htmlFor="address">
										Mobile Number
									</label>
									<input
										id="phone"
										className="input"
										type="tel"
										value={phone}
										onChange={phoneChangeHandler}
										required
									/>
								</div>
								{/* Phone */}

								<Button type="submit" color="success" fullwidth>
									Submit
								</Button>
							</form>
						</Card.Content>
					</Card>
				</Columns.Column>
			</Columns>
		</Container>
	);
};

export default compose(
	graphql(updateRegistration, { name: "updateRegistration" }),
	graphql(getRegistrationsQuery, { name: "getRegistrations" }),
	graphql(getRegistrationQuery, {
		options: props => {
			console.log(props.match.params.id);
			return {
				variables: { id: props.match.params.id }
			};
		}
	})
)(UpdateRegistration);
