import React, { useState } from "react";
import {
  Container,
  Columns,
  Card,
  Button,
  Heading
} from "react-bulma-components";
import { graphql } from "react-apollo";
import Swal from "sweetalert2";
import { flowRight as compose } from "lodash";
import { Link } from "react-router-dom";

import { getRoomCategoriesQuery } from "../queries/queries.js";
import { createRoomCategoryMutation } from "../queries/mutations.js";
import { deleteRoomCategory } from "../queries/mutations.js";

const RoomCategory = props => {
  console.log(props.data);
  const [roomType, setRoomType] = useState("");
  let data = props.data;

  const deleteClickHandler = e => {
    e.preventDefault();
    let deletesRoom = {
      id: e.target.value
    };
    console.log(deletesRoom);

    Swal.fire({
      title: "Are you sure?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonText: "Delete"
    }).then(result => {
      if (result.value) {
        props.deleteRoomCategory({
          variables: deletesRoom,
          refetchQueries: [{ query: getRoomCategoriesQuery }]
        });
        console.log(deletesRoom);
        Swal.fire("Deleted!", "success");
      }
    });
  };

  let roomTypeList;
  if (data.getRoomCategories === undefined) {
    roomTypeList = (
      <tr>
        <td>Fetching Room RoomCategory....</td>
      </tr>
    );
  } else {
    roomTypeList = data.getRoomCategories.map(roomType => {
      return (
        <tr key={roomType.id}>
          <td>{roomType.roomType}</td>
          <td>
            <Link to={"/admin/updateroomcategory/" + roomType.id}>
              <Button color="link" fullwidth>
                Update
              </Button>
            </Link>
            <Button
              value={roomType.id}
              onClick={deleteClickHandler}
              color="danger"
              fullwidth
            >
              Delete
            </Button>
          </td>
        </tr>
      );
    });
  }

  const roomCategoryChangeHandler = e => {
    setRoomType(e.target.value);
    console.log(roomType);
  };

  const addCategoryClickHandler = e => {
    let newRoomCategory = {
      roomType: roomType
    };

    console.log(newRoomCategory);

    props.createRoomCategory({
      variables: newRoomCategory,
      refetchQueries: [{ query: getRoomCategoriesQuery }]
    });

    // insert SWAL here
    Swal.fire({
      icon: "success",
      title: "Successfully added a new user"
    });
  };

  return (
    <Container>
      <Columns>
        <Columns.Column size={3}>
          <Card>
            <Card.Header>
              <Card.Header.Title>Add Room Category</Card.Header.Title>
            </Card.Header>
            <Card.Content>
              <form>
                {/* Position */}
                <div className="field">
                  <label htmlFor="tName">Room Category</label>
                  <input
                    id="tName"
                    className="input"
                    type="text"
                    onChange={roomCategoryChangeHandler}
                  />
                </div>
                <Button
                  onClick={addCategoryClickHandler}
                  color="success"
                  fullwidth
                >
                  Add Room Category
                </Button>
              </form>
            </Card.Content>
          </Card>
        </Columns.Column>
        <Columns.Column size={9}>
          <Card>
            <Card.Header>
              <Card.Header.Title>Room Category List</Card.Header.Title>
            </Card.Header>
            <Card.Content>
              <div className="table-container">
                <table className="table is-fullwidth">
                  <thead>
                    <tr>
                      <th>Room</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>{roomTypeList}</tbody>
                </table>
              </div>
            </Card.Content>
          </Card>
        </Columns.Column>
      </Columns>
    </Container>
  );
};

export default compose(
  graphql(getRoomCategoriesQuery),
  graphql(deleteRoomCategory, { name: "deleteRoomCategory" }),
  graphql(createRoomCategoryMutation, { name: "createRoomCategory" })
)(RoomCategory);
