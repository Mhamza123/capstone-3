import React, {useState} from "react";
import {
  Container,
  Columns,
  Card,
  Button,
  Loader
} from "react-bulma-components";
import { graphql } from "react-apollo";
import { flowRight as compose } from "lodash";
import { Link } from "react-router-dom";


import { getUserCategoriesQuery } from "../queries/queries.js";

const UserCategory = (props) => {
  console.log(props.data);
  const [userType, setUserType] = useState("Input team here");

  let data = props.data;
  let userTypeList;

  if(data.getUserCategories === undefined){
      userTypeList = (
        <tr>
          <td>Fetching Type of user....</td>
        </tr>
        )
  } else {
      userTypeList = data.getUserCategories.map(userType => {
        return (

            <tr key={userType.id} >
            <td>{userType.userType}</td>
              <td>
                <Button color="link"> Update </Button>
              </td>
            </tr>

          )
      })
  }

  return (
    <Container>
      <Columns>
        <Columns.Column size={3}>
          <Card>
            <Card.Header>
              <Card.Header.Title>Add User Role</Card.Header.Title>
            </Card.Header>
            <Card.Content>
              <form>
                {/* Position */}
                <div className="field">
                  <label htmlFor="tName">User Type</label>
                  <input
                    id="tName"
                    className="input"
                    type="text"
                    
                  />
            </div>
                <Button type="submit" color="success" fullwidth>
                  Add User Type
                </Button>
              </form>
            </Card.Content>
          </Card>
        </Columns.Column>
        <Columns.Column size={9}>
          <Card>
            <Card.Header>
              <Card.Header.Title>Type of User</Card.Header.Title>
            </Card.Header>
            <Card.Content>
              <div className="table-container">
                <table className="table is-fullwidth">
                  <thead>
                    <tr>
                      <th>Users</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    {userTypeList}
                  </tbody>
                </table>
              </div>
            </Card.Content>
          </Card>
        </Columns.Column>
      </Columns>
    </Container>
  );
};

export default graphql(getUserCategoriesQuery)(UserCategory);
