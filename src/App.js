import React from "react";
import logo from "./logo.svg";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import "react-bulma-components/dist/react-bulma-components.min.css";

import { ApolloProvider } from "react-apollo";
import ApolloClient from "apollo-boost";

/*React-router-dom*/
import { BrowserRouter, Route, Switch } from "react-router-dom";

import Client from "./components/Client.js";
import NavBar from "./components/NavBar.js";
import Reservation from "./components/Reservation.js";
import Login from "./components/Login.js";
import Registration from "./components/Registration.js";
import UserCategory from "./components/UserCategory.js";
import RoomCategory from "./components/RoomCategory.js";
import Room from "./components/Room.js";
import Homepage from "./components/Homepage.js";
import WorkSpace from "./components/WorkSpace.js";
import AddWorkSpace from "./components/AddWorkSpace.js";
import UpdateRegistration from "./components/UpdateRegistration.js";
import UpdateRoomCategory from "./components/UpdateRoomCategory.js";

const client = new ApolloClient({
  uri: "http://localhost:4000/graphql",
  onError: e => {
    console.log(e);
  }
});

function App() {
  return (
    <ApolloProvider client={client}>
      <BrowserRouter>
        <NavBar />
        <Switch>
          {/*Client*/}
          <Route exact path="/" component={Homepage} />
          <Route path="/workspace" component={WorkSpace} />
          <Route path="/workspace/reservation" component={Reservation} />
          <Route path="/room" component={Room} />
          {/*Entry*/}
          <Route path="/login" component={Login} />
          <Route path="/registration" component={Registration} />
          {/*Backend*/}
          <Route path="/admin/client" component={Client} />
          <Route
            path="/admin/updateregistration/:id"
            component={UpdateRegistration}
          />
          <Route path="/addworkspace" component={AddWorkSpace} />
          <Route path="/admin/usercategory" component={UserCategory} />
          <Route path="/admin/room" component={Room} />
          <Route
            path="/admin/updateroomcategory/:id"
            component={UpdateRoomCategory}
          />
          <Route path="/admin/roomcategory" component={RoomCategory} />
        </Switch>
      </BrowserRouter>
    </ApolloProvider>
  );
}

export default App;
