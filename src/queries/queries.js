import { gql } from "apollo-boost";

const getUserCategoriesQuery = gql`
  {
    getUserCategories {
      id
      userType
      registration {
        firstname
      }
    }
  }
`;

const getRegistrationsQuery = gql`
  {
    getRegistrations {
      id
      firstname
      lastname
      email
      phone
      position
      address
      username
      password
      userType {
        id
        userType
      }
    }
  }
`;

const getRoomsQuery = gql`
  {
    getRooms {
      id
      roomNumber
      numOfPeople
      roomType {
        id
        roomType
      }
    }

    getRoomCategories {
      id
      roomType
    }
  }
`;

const getRoomCategoriesQuery = gql`
  {
    getRoomCategories {
      id
      roomType
    }
  }
`;

const getRegistrationQuery = gql`
  query($id: String!) {
    getRegistration(id: $id) {
      id
      firstname
      lastname
      email
      phone
      position
      address
    }
  }
`;

const getRoomQuery = gql`
  query($id: ID) {
    getRoom(id: $id) {
      id
      roomNumber
      numOfPeople
      roomType {
        id
        roomType
      }
    }
  }
`;

const getRoomCategoryQuery = gql`
  query($id: String!) {
    getRoomCategory(id: $id) {
      id
      roomType
    }
  }
`;

export {
  getUserCategoriesQuery,
  getRegistrationsQuery,
  getRoomCategoriesQuery,
  getRegistrationQuery,
  getRoomCategoryQuery,
  getRoomsQuery,
  getRoomQuery
};
