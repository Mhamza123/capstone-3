import { gql } from "apollo-boost";

const createRegistrationMutation = gql`
	mutation(
		$firstname: String!
		$lastname: String!
		$email: String!
		$address: String!
		$phone: String!
		$username: String!
		$password: String!
	) {
		createRegistration(
			firstname: $firstname
			lastname: $lastname
			email: $email
			address: $address
			phone: $phone
			username: $username
			password: $password
		) {
			firstname
			lastname
			email
			address
			phone
			username
			password
		}
	}
`;

const createRoomMutation = gql`
	mutation(
		$roomNumber: roomNumber
		$roomCategId: roomCategId
		$numOfPeople: numOfPeople
	) {
		createRoom(
			roomNumber: $roomNumber
			roomCategId: $roomCategId
			numOfPeople: $numOfPeople
		) {
			roomNumber
			roomCategId
			numOfPeople
		}
	}
`;

const createRoomCategoryMutation = gql`
	mutation($roomType: String) {
		createRoomCategory(roomType: $roomType) {
			roomType
		}
	}
`;

const updateRegistration = gql`
	mutation(
		$id: ID!
		$firstname: String!
		$lastname: String!
		$email: String!
		$address: String!
		$phone: String!
	) {
		updateRegistration(
			id: $id
			firstname: $firstname
			lastname: $lastname
			email: $email
			address: $address
			phone: $phone
		) {
			firstname
			lastname
			email
			address
			phone
		}
	}
`;

const updateRoom = gql`
	mutation(
		$id: ID
		$roomNumber: roomNumber
		$roomCategId: roomCategId
		$numOfPeople: numOfPeople
	) {
		updateRoom(
			id: $id
			roomNumber: $roomNumber
			roomCategId: $roomCategId
			numOfPeople: $numOfPeople
		) {
			roomNumber
			roomCategId
			numOfPeople
		}
	}
`;

const updateRoomCategory = gql`
	mutation($id: ID!, $roomType: String) {
		updateRoomCategory(id: $id, roomType: $roomType) {
			roomType
		}
	}
`;

const deleteRegistration = gql`
	mutation($id: ID) {
		deleteRegistration(id: $id) {
			id
			firstname
			lastname
			email
			address
			phone
		}
	}
`;

const deleteRoom = gql`
	mutation($id: ID!) {
		deleteRoom(id: $id) {
			id
			roomNumber
			roomCategId
			numOfPeople
		}
	}
`;

const deleteRoomCategory = gql`
	mutation($id: ID!) {
		deleteRoomCategory(id: $id) {
			id
			roomType
		}
	}
`;

export {
	createRegistrationMutation,
	createRoomMutation,
	createRoomCategoryMutation,
	updateRegistration,
	updateRoom,
	updateRoomCategory,
	deleteRegistration,
	deleteRoom,
	deleteRoomCategory
};
